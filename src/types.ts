export interface CodeStoreState {
  name: string;
  code: string;
}
